{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

unit SmartCodeCompletion.Expert;

interface

{$I SmartCodeCompletion.inc}

{$IFDEF DEBUG}
  {$DEFINE WRAP_ALL}
{$ENDIF}

uses
  SysUtils,
  StrUtils,
  ToolsAPI,
  Windows,
  Graphics,
  Levenshtein,
  SmartCodeCompletion.Logging,
  SmartCodeCompletion.Wrapper;

type
  TSmartCodeCompletion = class
  private
    fBitmap: HBITMAP;
    fAboutBoxIndex: Integer;
  public
    constructor Create;
  end;

  TSmartCodeInsightPrimaryManagerWrapper = class(
    TAggregatedIDispatchWrapper<IOTAPrimaryCodeInsightManager>,
    IOTAPrimaryCodeInsightManager)
  public
    function GetContext: TOTACodeCompletionContext;
  end;

  TSmartCodeInsightCustomDrawCodeInsightViewerWrapper = class(
    TAggregatedInterfaceWrapper<INTACustomDrawCodeInsightViewer>,
    INTACustomDrawCodeInsightViewer)
  public
    procedure DrawLine(Index: Integer; Canvas: TCanvas; var Rect: TRect;
      DrawingHintText: Boolean; DoDraw: Boolean; var DefaultDraw: Boolean);
  end;

  TSmartCodeInsightManagerWrapper = class(TInterfaceWrapper<IOTACodeInsightManager>,
    IOTACodeInsightManager, IOTACodeInsightManager90
{$IFDEF WRAP_ALL}
    ,IOTAPrimaryCodeInsightManager
    ,INTACustomDrawCodeInsightViewer
{$ENDIF}
    )
  private
    fInstance90: IOTACodeInsightManager90;
    /// <summary>
    ///   If disabled the wrapper is skipped but the code completion continues
    ///   to work.
    /// </summary>
    fWrapperEnabled: Boolean;
    fRunning: Boolean;
    /// <summary>
    ///   Cached wrapper instance
    /// </summary>
    fCachedList: IOTACodeInsightSymbolList;
    /// <summary>
    ///   Last instance to wrap
    /// </summary>
    fLastList: Pointer {IOTACodeInsightSymbolList};
{$IFDEF WRAP_ALL}
    fPrimaryManager: IOTAPrimaryCodeInsightManager;
    fCustomDrawCodeInsightViewer: INTACustomDrawCodeInsightViewer;
{$ENDIF}
  public
    procedure AfterConstruction; override;

    // IOTACodeInsightManager100
    function GetName: string;
    function GetIDString: string;
    function GetEnabled: Boolean;
    procedure SetEnabled(Value: Boolean);
    function EditorTokenValidChars(PreValidating: Boolean): TSysCharSet;
    procedure AllowCodeInsight(var Allow: Boolean; const Key: Char);
    function PreValidateCodeInsight(const Str: string): Boolean;
    function IsViewerBrowsable(Index: Integer): Boolean;
    function GetMultiSelect: Boolean;
    procedure GetSymbolList(out SymbolList: IOTACodeInsightSymbolList);
    procedure OnEditorKey(Key: Char; var CloseViewer: Boolean; var Accept: Boolean);
    function HandlesFile(const AFileName: string): Boolean;
    function GetLongestItem: string;
    procedure GetParameterList(out ParameterList: IOTACodeInsightParameterList);
    procedure GetCodeInsightType(AChar: Char; AElement: Integer; out CodeInsightType: TOTACodeInsightType;
      out InvokeType: TOTAInvokeType);
    function InvokeCodeCompletion(HowInvoked: TOTAInvokeType; var Str: string): Boolean;
    function InvokeParameterCodeInsight(HowInvoked: TOTAInvokeType; var SelectedIndex: Integer): Boolean;
    procedure ParameterCodeInsightAnchorPos(var EdPos: TOTAEditPos);
    function ParameterCodeInsightParamIndex(EdPos: TOTAEditPos): Integer;
    function GetHintText(HintLine, HintCol: Integer): string;
    function GotoDefinition(out AFileName: string; out ALineNum: Integer; Index: Integer = -1): Boolean;
    procedure Done(Accepted: Boolean; out DisplayParams: Boolean);

    property Name: string read GetName;
    property MultiSelect: Boolean read GetMultiSelect;
    property Enabled: Boolean read GetEnabled write SetEnabled;

    // IOTACodeInsightManager
    function GetOptionSetName: string;

    // IOTACodeInsightManager90
    function GetHelpInsightHtml: WideString;

{$IFDEF WRAP_ALL}
    // IOTAPrimaryCodeInsightManager
    property PrimaryManager: IOTAPrimaryCodeInsightManager
      read fPrimaryManager implements IOTAPrimaryCodeInsightManager;

    // IOTAPrimaryCodeInsightManager
    property CustomDrawCodeInsightViewer: INTACustomDrawCodeInsightViewer
      read fCustomDrawCodeInsightViewer implements INTACustomDrawCodeInsightViewer;
{$ENDIF}
  end;

  TSmartCodeInsightSymbolListWrapper = class(TInterfaceWrapper<IOTACodeInsightSymbolList>,
    IOTACodeInsightSymbolList, IOTACodeInsightSymbolList80)
  private
    fInstance80: IOTACodeInsightSymbolList80;
  public
    procedure AfterConstruction; override;

    // IOTACodeInsightSymbolList
    procedure Clear;
    function GetCount: Integer;
    function GetSymbolIsReadWrite(I: Integer): Boolean;
    function GetSymbolIsAbstract(I: Integer): Boolean;
    function GetViewerSymbolFlags(I: Integer): TOTAViewerSymbolFlags;
    function GetViewerVisibilityFlags(I: Integer): TOTAViewerVisibilityFlags;
    function GetProcDispatchFlags(I: Integer): TOTAProcDispatchFlags;
    procedure SetSortOrder(const Value: TOTASortOrder);
    function GetSortOrder: TOTASortOrder;
    function FindIdent(const AnIdent: string): Integer;
    function FindSymIndex(const Ident: string; var Index: Integer): Boolean;
    procedure SetFilter(const FilterText: string);
    function GetSymbolText(Index: Integer): string;
    function GetSymbolTypeText(Index: Integer): string;
    function GetSymbolClassText(I: Integer): string;

    // IOTACodeInsightSymbolList80
    function GetSymbolDocumentation(I: Integer): string;
  end;

implementation

{ TSmartCodeCompletion }

constructor TSmartCodeCompletion.Create;
type
  TVersionInfo = record
    major: Integer;
    minor: Integer;
    patch: Integer;
    build: Integer;
  end;

  procedure GetVersionInfo(var versionInfo: TVersionInfo);
  var
    buffer: array[0..MAX_PATH] of Char;
    infoSize: DWORD;
    info: Pointer;
    valueSize: DWORD;
    value: PVSFixedFileInfo;
    dummy: DWORD;
  begin
    GetModuleFileName(hInstance, buffer, MAX_PATH);
    infoSize := GetFileVersionInfoSize(buffer, dummy);
    if infoSize <> 0 then
    begin
      GetMem(info, infoSize);
      try
        GetFileVersionInfo(buffer, 0, infoSize, info);
        VerQueryValue(info, '\', Pointer(value), valueSize);
        with value^ Do
        begin
          versionInfo.major := dwFileVersionMS shr 16;
          versionInfo.minor := dwFileVersionMS and $FFFF;
          versionInfo.patch := dwFileVersionLS shr 16;
          versionInfo.build := dwFileVersionLS and $FFFF;
        end;
      finally
        FreeMem(info, infoSize);
      end;
    end;
  end;

  function findManager(out manager: IOTACodeInsightManager; out index: Integer;
    const codeInsightServices: IOTACodeInsightServices; const id: string): Boolean;
  var
    i: Integer;
  begin
    for i := 0 to codeInsightServices.CodeInsightManagerCount - 1 do
    begin
      manager := codeInsightServices.CodeInsightManager[i];
      if manager.GetIDString = id then
      begin
        index := i;
        Exit(True);
      end;
    end;
    manager := nil;
    index := -1;
    Result := False;
  end;

const
  ProductName = 'SmartCodeCompletion %d.%d';
  ProductDescription = '';
var
  //icon: TIcon;
  fileVersion: TVersionInfo;
  productVersion: string;

  codeInsightServices: IOTACodeInsightServices;
  manager: IOTACodeInsightManager;
  index: Integer;
begin
  inherited Create;

  codeInsightServices := BorlandIDEServices as IOTACodeInsightServices;
  if not findManager(manager, index, codeInsightServices,
    'Borland.CodeInsight.Pascal') then
      Abort;

  codeInsightServices.RemoveCodeInsightManager(index);
  codeInsightServices.AddCodeInsightManager(
    TSmartCodeInsightManagerWrapper.Create(manager));

  GetVersionInfo(fileVersion);
  productVersion := Format(ProductName, [
    fileVersion.major, fileVersion.minor, fileVersion.patch, fileVersion.build]);
  //fBitmap := LoadBitmap(hInstance, 'SmartCodeCompletionLogo');
  fAboutBoxIndex := (BorlandIDEServices as IOTAAboutBoxServices).AddPluginInfo(
    productVersion, ProductDescription, fBitmap);
  SplashScreenServices.AddPluginBitmap(productVersion, fBitmap);
end;

{ TSmartCodeInsightManagerWrapper }

procedure TSmartCodeInsightManagerWrapper.AfterConstruction;
begin
  fInstance.QueryInterface(IOTACodeInsightManager90, fInstance90);
{$IFDEF WRAP_ALL}
  TryAddAggregate<IOTAPrimaryCodeInsightManager>(fInstance, fPrimaryManager,
    TSmartCodeInsightPrimaryManagerWrapper);
  TryAddAggregate<INTACustomDrawCodeInsightViewer>(fInstance,
    fCustomDrawCodeInsightViewer,
    TSmartCodeInsightCustomDrawCodeInsightViewerWrapper);
{$ENDIF}
  fWrapperEnabled := True;
  // Call after we're initialized to maintain RefCount during initialization
  inherited;
end;

procedure TSmartCodeInsightManagerWrapper.AllowCodeInsight(var Allow: Boolean;
  const Key: Char);
begin
  fInstance.AllowCodeInsight(Allow, Key);
  {$IFDEF DEBUG}Trace('AllowCodeInsight', BoolToStr(Allow, True) + ': ' + Key);{$ENDIF}
  fRunning := Allow and fWrapperEnabled;
end;

procedure TSmartCodeInsightManagerWrapper.Done(Accepted: Boolean;
  out DisplayParams: Boolean);
begin
  {$IFDEF DEBUG}Trace('Done', BoolToStr(Accepted, True));{$ENDIF}
  fInstance.Done(Accepted, DisplayParams);
  // fRunning := False;
  // No need to clear the cache, Delphi use one persistent object anyway
  // fCachedList := nil;
  // fLastList := nil;
end;

function TSmartCodeInsightManagerWrapper.EditorTokenValidChars(
  PreValidating: Boolean): TSysCharSet;
begin
  Result := fInstance.EditorTokenValidChars(PreValidating)
end;

procedure TSmartCodeInsightManagerWrapper.GetCodeInsightType(AChar: Char;
  AElement: Integer; out CodeInsightType: TOTACodeInsightType;
  out InvokeType: TOTAInvokeType);
begin
  {$IFDEF DEBUG}Trace('GetCodeInsightType', AChar);{$ENDIF}
  fInstance.GetCodeInsightType(AChar, AElement, CodeInsightType, InvokeType);
end;

function TSmartCodeInsightManagerWrapper.GetEnabled: Boolean;
begin
  Result := fInstance.GetEnabled;
end;

function TSmartCodeInsightManagerWrapper.GetHelpInsightHtml: WideString;
begin
  if Assigned(fInstance90) then
    Result := fInstance90.GetHelpInsightHtml
  else
    Result := '';
end;

function TSmartCodeInsightManagerWrapper.GetHintText(HintLine,
  HintCol: Integer): string;
begin
  Result := fInstance.GetHintText(HintLine, HintCol);
  {$IFDEF DEBUG}Trace('GetHintText', Result);{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.GetIDString: string;
begin
  Result := fInstance.GetIDString;
end;

function TSmartCodeInsightManagerWrapper.GetLongestItem: string;
begin
  Result := fInstance.GetLongestItem;
end;

function TSmartCodeInsightManagerWrapper.GetMultiSelect: Boolean;
begin
  Result := fInstance.GetMultiSelect;
end;

function TSmartCodeInsightManagerWrapper.GetName: string;
begin
  Result := fInstance.GetName;
end;

function TSmartCodeInsightManagerWrapper.GetOptionSetName: string;
begin
  Result := fInstance.GetOptionSetName;
end;

procedure TSmartCodeInsightManagerWrapper.GetParameterList(
  out ParameterList: IOTACodeInsightParameterList);
begin
  {$IFDEF DEBUG}Trace('GetParameterList');{$ENDIF}
  fInstance.GetParameterList(ParameterList);
end;

procedure TSmartCodeInsightManagerWrapper.GetSymbolList(
  out SymbolList: IOTACodeInsightSymbolList);
begin
  // {$IFDEF DEBUG}Trace('GetSymbolList');{$ENDIF}
  fInstance.GetSymbolList(SymbolList);
  if fRunning then
  begin
    if fLastList = Pointer(SymbolList) then
    begin
      // {$IFDEF DEBUG}Trace('Reused from cache');{$ENDIF}
      Assert(Assigned(fCachedList));
    end
    else
    begin
      {$IFDEF DEBUG}Trace('New list');{$ENDIF}
      fLastList := Pointer(SymbolList);
      fCachedList := TSmartCodeInsightSymbolListWrapper.Create(SymbolList);
    end;
    SymbolList := fCachedList;
  end;
end;

function TSmartCodeInsightManagerWrapper.GotoDefinition(out AFileName: string;
  out ALineNum: Integer; Index: Integer): Boolean;
begin
  Result := fInstance.GotoDefinition(AFileName, ALineNum, Index);
  {$IFDEF DEBUG}Trace('GotoDefinition', AFileName);{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.HandlesFile(
  const AFileName: string): Boolean;
begin
  Result := fInstance.HandlesFile(AFileName);
end;

function TSmartCodeInsightManagerWrapper.InvokeCodeCompletion(
  HowInvoked: TOTAInvokeType; var Str: string): Boolean;
begin
  Result := fInstance.InvokeCodeCompletion(HowInvoked, Str);
  {$IFDEF DEBUG}Trace('InvokeCodeCompletion', Str);{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.InvokeParameterCodeInsight(
  HowInvoked: TOTAInvokeType; var SelectedIndex: Integer): Boolean;
begin
  Result := fInstance.InvokeParameterCodeInsight(HowInvoked, SelectedIndex);
  {$IFDEF DEBUG}Trace('InvokeParameterCodeInsight', BoolToStr(Result, True));{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.IsViewerBrowsable(
  Index: Integer): Boolean;
begin
  Result := fInstance.IsViewerBrowsable(Index);
end;

procedure TSmartCodeInsightManagerWrapper.OnEditorKey(Key: Char;
  var CloseViewer, Accept: Boolean);
begin
  {$IFDEF DEBUG}Trace('OnEditorKey', Key);{$ENDIF}
  fInstance.OnEditorKey(Key, CloseViewer, Accept);
end;

procedure TSmartCodeInsightManagerWrapper.ParameterCodeInsightAnchorPos(
  var EdPos: TOTAEditPos);
begin
  fInstance.ParameterCodeInsightAnchorPos(EdPos);
end;

function TSmartCodeInsightManagerWrapper.ParameterCodeInsightParamIndex(
  EdPos: TOTAEditPos): Integer;
begin
  Result := fInstance.ParameterCodeInsightParamIndex(EdPos);
  {$IFDEF DEBUG}Trace('ParameterCodeInsightParamIndex', IntToStr(Result));{$ENDIF}
end;

function TSmartCodeInsightManagerWrapper.PreValidateCodeInsight(
  const Str: string): Boolean;
begin
  {$IFDEF DEBUG}Trace('PreValidateCodeInsight', Str);{$ENDIF}
  Result := fInstance.PreValidateCodeInsight(Str);
end;

procedure TSmartCodeInsightManagerWrapper.SetEnabled(Value: Boolean);
begin
  fInstance.SetEnabled(Value);
end;

{ TSmartCodeInsightPrimaryManagerWrapper }

function TSmartCodeInsightPrimaryManagerWrapper.GetContext: TOTACodeCompletionContext;
begin
  Result := fInstance.GetContext;
end;

{ TSmartCodeInsightSymbolListWrapper }

procedure TSmartCodeInsightSymbolListWrapper.AfterConstruction;
begin
  inherited;
  fInstance.QueryInterface(IOTACodeInsightSymbolList80, fInstance80);
end;

procedure TSmartCodeInsightSymbolListWrapper.Clear;
begin
  fInstance.Clear;
end;

function TSmartCodeInsightSymbolListWrapper.FindIdent(
  const AnIdent: string): Integer;
begin
  Result := fInstance.FindIdent(AnIdent);
  {$IFDEF DEBUG}Trace('FindIdent', AnIdent + ': ' + IntToStr(Result));{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.FindSymIndex(const Ident: string;
  var Index: Integer): Boolean;
var
  lCount, i, iBest: Integer;
  d, best: Double;
  s: string;
begin
  // Do not call original it would find the correct symbol by leading string
  // comparison and return true even if the leading string doesn't match
  // completely.
  // Result := fInstance.FindSymIndex(Ident, Index);
  // if Result then
  //   Exit;

  if Ident = '' then
    Exit(False);

  lCount := fInstance.Count;
  if lCount = 0 then
    Exit(False);

  iBest := -1;
  best := -1;

  for i := 0 to lCount - 1 do
  begin
    s := fInstance.SymbolText[i];

    d := StringSimilarityRatio(Ident, s, True);
    if d > best then
    begin
      best := d;
      iBest := i;
    end;

    if StartsText(Ident, s) then
    begin
      // Set to high rank while giving a bump to the prefix while also
      // maintaining similarity ratio at some degree.
      // Note: if this doesn't work compare the similarity as
      // `Length(Ident) / Length(s)` that should put shortest macthes first.
      d := 0.8 + (d * 0.2);
      if d > best then
      begin
        iBest := i;
        best := d;
        // Don't break here, direct match should always win
      end;
    end;
  end;

  Result := iBest >= 0;
  if Result then
    Index := iBest;

{$IFDEF DEBUG}
  if iBest >= 0 then
    s := fInstance.SymbolText[iBest];
  Trace('FindSymIndex', Ident + ' > ' + s + ': ' + BoolToStr(Result, True)  + ' @' + IntToStr(Index) + ' ' + FloatToStr(best * 100) + '%');
{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.GetCount: Integer;
begin
  Result := fInstance.GetCount;
end;

function TSmartCodeInsightSymbolListWrapper.GetProcDispatchFlags(
  I: Integer): TOTAProcDispatchFlags;
begin
  Result := fInstance.GetProcDispatchFlags(I);
end;

function TSmartCodeInsightSymbolListWrapper.GetSortOrder: TOTASortOrder;
begin
  Result := fInstance.GetSortOrder;
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolClassText(
  I: Integer): string;
begin
  Result := fInstance.GetSymbolClassText(I);
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolDocumentation(
  I: Integer): string;
begin
  if Assigned(fInstance80) then
    Result := fInstance80.GetSymbolDocumentation(I)
  else
    Result := '';
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolIsAbstract(
  I: Integer): Boolean;
begin
  Result := fInstance.GetSymbolIsAbstract(I);
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolIsReadWrite(
  I: Integer): Boolean;
begin
  Result := fInstance.GetSymbolIsReadWrite(I);
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolText(
  Index: Integer): string;
begin
  Result := fInstance.GetSymbolText(Index);
  // {$IFDEF DEBUG}Trace('GetSymbolText', IntToStr(Index) + ': ' + Result);{$ENDIF}
end;

function TSmartCodeInsightSymbolListWrapper.GetSymbolTypeText(
  Index: Integer): string;
begin
  Result := fInstance.GetSymbolTypeText(Index)
end;

function TSmartCodeInsightSymbolListWrapper.GetViewerSymbolFlags(
  I: Integer): TOTAViewerSymbolFlags;
begin
  Result := fInstance.GetViewerSymbolFlags(I);
end;

function TSmartCodeInsightSymbolListWrapper.GetViewerVisibilityFlags(
  I: Integer): TOTAViewerVisibilityFlags;
begin
  Result := fInstance.GetViewerVisibilityFlags(I);
end;

procedure TSmartCodeInsightSymbolListWrapper.SetFilter(
  const FilterText: string);
begin
  {$IFDEF DEBUG}Trace('SetFilter', FilterText);{$ENDIF}
  // fInstance.SetFilter(FilterText);
  // Show all items all the time FindSymIndex will find the proper item to focus
  fInstance.SetFilter('');
end;

procedure TSmartCodeInsightSymbolListWrapper.SetSortOrder(
  const Value: TOTASortOrder);
begin
  fInstance.SetSortOrder(Value);
end;

{ TSmartCodeInsightCustomDrawCodeInsightViewerWrapper }

procedure TSmartCodeInsightCustomDrawCodeInsightViewerWrapper.DrawLine(
  Index: Integer; Canvas: TCanvas; var Rect: TRect; DrawingHintText,
  DoDraw: Boolean; var DefaultDraw: Boolean);
begin
  fInstance.DrawLine(Index, Canvas, Rect, DrawingHintText, DoDraw, DefaultDraw);
end;

end.

// Unknown interfaces
// {A84C137A-F375-478E-AD0C-479B707A22EB} ???
// {0F6FC4F5-0E9A-44C0-B684-57A6F7C76AF3} ??? (TSmartCodeInsightSymbolListWrapper)


{******************************************************************************}
{                                                                              }
{            SmartCodeCompletion                                               }
{                                                                              }
{            Copyright (c) 2015 - 2016 Jan Rames                               }
{                                                                              }
{******************************************************************************}
{                                                                              }
{            This Source Code Form is subject to the terms of the              }
{                                                                              }
{                       Mozilla Public License, v. 2.0.                        }
{                                                                              }
{            If a copy of the MPL was not distributed with this file,          }
{            You can obtain one at http://mozilla.org/MPL/2.0/.                }
{                                                                              }
{******************************************************************************}

unit SmartCodeCompletion.Wrapper;

interface

{$I SmartCodeCompletion.inc}

uses
  SysUtils,
  Math,
  TypInfo,
  Windows,
  SmartCodeCompletion.Logging;

type
  TAggregatedInterfaceWrapper = class(TAggregatedObject)
  protected
    constructor Create(const controller: IInterface;
      const instance: IInterface); overload; virtual;
  end;

  TAggregatedInterfaceWrapperClass = class of TAggregatedInterfaceWrapper;

  TAggregatedInterfaceWrapper<T: IInterface> = class(TAggregatedInterfaceWrapper)
  strict protected
    fInstance: T;
  protected
    constructor Create(const controller: IInterface;
      const instance: IInterface); override;
  end;

  TInterfaceWrapper = class(TInterfacedObject)
  private type
    PInterafce = ^IInterface;
    TAggregate = record
      field: PInterafce;
      instance: TAggregatedObject;
    end;
  private
    fAggregates: TArray<TAggregate>;
  strict protected
    procedure TryAddAggregate<T: IInterface>(const instance: IInterface;
      var field: T; aggregateClass: TAggregatedInterfaceWrapperClass);
  public
    destructor Destroy; override;
  end;

  TInterfaceWrapper<T: IInterface> = class(TInterfaceWrapper, IInterface)
  strict protected
    fInstance: T;
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    constructor Create(const instance: T);
  end;

  TAggregatedIDispatchWrapper<T: IDispatch> = class(TAggregatedInterfaceWrapper<T>,
    IDispatch)
  public
    function GetTypeInfoCount(out Count: Integer): HResult; stdcall;
    function GetTypeInfo(Index, LocaleID: Integer; out TypeInfo): HResult; stdcall;
    function GetIDsOfNames(const IID: TGUID; Names: Pointer;
      NameCount, LocaleID: Integer; DispIDs: Pointer): HResult; stdcall;
    function Invoke(DispID: Integer; const IID: TGUID; LocaleID: Integer;
      Flags: Word; var Params; VarResult, ExcepInfo, ArgErr: Pointer): HResult; stdcall;
  end;

function IsDirectReference(const inst: IInterface; typeInfo: PTypeInfo): Boolean;
function GetGUID(typeInfo: PTypeInfo): TGUID;

implementation

function IsDirectReference(const inst: IInterface; typeInfo: PTypeInfo): Boolean;
var
  intf: IInterface;
begin
  Result := Supports(inst, GetGUID(typeInfo), intf)
    and (inst = intf);
end;

function GetGUID(typeInfo: PTypeInfo): TGUID;
begin
  Assert(typeInfo^.Kind = tkInterface);
  Result := GetTypeData(typeInfo)^.Guid;
end;

{ TAggregatedInterfaceWrapper }

constructor TAggregatedInterfaceWrapper.Create(const controller,
  instance: IInterface);
begin
  inherited Create(controller);
end;

{ TAggregatedInterfaceWrapper<T> }

constructor TAggregatedInterfaceWrapper<T>.Create(const controller: IInterface;
  const instance: IInterface);
begin
  inherited;
  Assert(IsDirectReference(instance, TypeInfo(T)));
  IInterface(fInstance) := instance;
end;

{ TInterfaceWrapper }

destructor TInterfaceWrapper.Destroy;
var
  aggregate: TAggregate;
begin
  for aggregate in fAggregates do
  begin
    // Clear weak reference (see TryAddAggregate)
    Pointer(aggregate.field^) := nil;
    aggregate.instance.Free;
  end;
  fAggregates := nil;

  inherited;
end;

procedure TInterfaceWrapper.TryAddAggregate<T>(const instance: IInterface;
  var field: T; aggregateClass: TAggregatedInterfaceWrapperClass);
var
  IID: TGUID;
  delegate: IInterface;
  aggregate: TAggregate;
begin
  aggregate.field := @field;
  Assert(InRange(NativeInt(aggregate.field), NativeInt(Self),
    NativeInt(Self) + InstanceSize), 'field expected');
  IID := GetGUID(TypeInfo(T));
  if Succeeded(instance.QueryInterface(IID, delegate)) then
  begin
    aggregate.instance := aggregateClass.Create(Self, delegate);
    try
      aggregate.instance.GetInterface(IID, aggregate.field^);
      Assert(Assigned(aggregate.field^));
      // Decrement _RefCount aggregates increment controller's RefCount so this
      // would create a cycle preventing us from releasing this instance.
      // The reference is cleared prior callig CleanupInstance by our dtor.
      Assert(Self.RefCount > 1);
      aggregate.field^._Release;
      Assert(aggregate.field^ = IInterface(field));
      SetLength(fAggregates, Length(fAggregates) + 1);
      fAggregates[High(fAggregates)] := aggregate;
    except
      aggregate.instance.Free;
    end;
  end;
end;

{ TInterfaceWrapper<T> }

constructor TInterfaceWrapper<T>.Create(const instance: T);
begin
  inherited Create;
  fInstance := instance;
end;

function TInterfaceWrapper<T>.QueryInterface(const IID: TGUID;
  out Obj): HResult;
begin
  Result := inherited;
  if not Succeeded(Result) then
  begin
    Result := fInstance.QueryInterface(IID, obj);
    {$IFDEF DEBUG}Trace('Wrapped instance returned: ', ClassName + ' - ' + IID.ToString);{$ENDIF}
  end;
end;

{ TAggregatedIDispatchWrapper<T> }

function TAggregatedIDispatchWrapper<T>.GetIDsOfNames(const IID: TGUID;
  Names: Pointer; NameCount, LocaleID: Integer; DispIDs: Pointer): HResult;
begin
  Result := fInstance.GetIDsOfNames(IID, Names, NameCount, LocaleID, DispIDs);
end;

function TAggregatedIDispatchWrapper<T>.GetTypeInfo(Index, LocaleID: Integer;
  out TypeInfo): HResult;
begin
  Result := fInstance.GetTypeInfo(Index, LocaleID, TypeInfo);
end;

function TAggregatedIDispatchWrapper<T>.GetTypeInfoCount(
  out Count: Integer): HResult;
begin
  Result := fInstance.GetTypeInfoCount(Count);
end;

function TAggregatedIDispatchWrapper<T>.Invoke(DispID: Integer; const IID: TGUID;
  LocaleID: Integer; Flags: Word; var Params; VarResult, ExcepInfo,
  ArgErr: Pointer): HResult;
begin
  Result := fInstance.Invoke(DispID, IID, LocaleID, Flags, Params, VarResult,
    ExcepInfo, ArgErr);
end;

end.

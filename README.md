# SmartCodeCompletion #

BDS expert that uses Levenshtein distance to compute code completion auto-suggestions, this allows to find partial or misspelled matches.

License
-------
This Source Code Form is subject to the terms of the Mozilla Public License,
v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain
one at http://mozilla.org/MPL/2.0/.
